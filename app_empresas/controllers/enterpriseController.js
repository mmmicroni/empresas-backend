const db = require("../models");
const Enterprise = db.enterprises;
const Op = db.Op;

// criar nova Enterprise
exports.create = (req, res) => {
  if (!req.body.enterprise_name) {
    res.status(400).send({
      message: "FAVOR PREENCHER CORRETAMENTE OS CAMPOS!"
    });
    return;
  }

  // criar uma Enterprise
  const enterprise = {
    enterprise_name: req.body.enterprise_name,
    description: req.body.description,
  };

  // salvar Enterprise
  Enterprise.create(enterprise)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "ERRO AO CRIAR A ENTERPRISE"
      });
    });
};

// buscar todas as Enterprise
exports.findAll = (req, res) => {
  const enterprise_name = req.query.enterprise_name;
  var condition = enterprise_name ? { enterprise_name: { [Op.like]:'%${enterprise_name}%'} } : null;

  Enterprise.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.send(500).send({
        message: err.message || "ERRO AO BUSCAS AS ENTERPRISES."
      });
    });
};

// BUSCAR UMA ENTERPRISE PELO SEU ID
exports.findOne = (req, res) => {
  const id = req.params.id;

  Enterprise.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: 'ERRO AO BUSCA ENTERPRISE COM O ID = ${id}'
      });
    });
};

//BUSCAR UMA ENTERPRISE POR FILTRO
//testando voltar e corrigir
exports.findAllFilter= (req, res) => {
  const id_type = req.params.enterprise_types;
  const name = req.params.name;
  
  
  Enterprise.findAll({ where: {enterprise_type_id: id_type, enterprise_name: name} })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "ERRO AO BUCAS ENTERPRISES"
      });
    });
};

