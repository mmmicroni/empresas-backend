const config = require("../config/config");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require("../models");
const User = db.user;
const Perfil = db.perfil;
const Op = db.Op;

exports.signup = (req, res) => {
  // Salvar user
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  })
    .then(user => {
      if (req.body.perfis) {
        Perfil.findAll({
          where: {
            name: {
              [Op.or]: req.body.perfis
            }
          }
        }).then(perfis => {
          user.setPerfils(perfis).then(() => {
            res.send({ message: "USER REGISTRADO COM SUCESSO!" });
          });
        });
      } else {
        user.setPerfils([1]).then(() => {
          res.send({ message: "USER REGISTRADO COM SUCESSO!" });
        });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: " USER NÃO ENCONTRADO" });
      }

      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "SENHA INVALIDA!"
        });
      }

      let token = jwt.sign({ id: user.id }, config.auth.secret, {
        expiresIn: 86400 // 24 HORAS
      });

      res.header("accessToken", token);
      res.header("Client",  user.id);
      res.header("uid", user.email);

      let authorities = [];
      user.getPerfils().then(perfis => {
        for (let i = 0; i < perfis.length; i++) {
          authorities.push("PERFIL_" + perfis[i].name.toUpperCase());
        }

        res.status(200).send({
          id: user.id,
          username: user.username,
          email: user.email,
          city: user.city,
          country: user.country,
          balance: user.balance,
          photo: user.photo,
          portfolio_value: user.portfolio_value,
          first_access: user.first_access,
          super_angel: user.super_angel,
          //roles: authorities,
          //accessToken: token,
          
        });
       
      });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};
