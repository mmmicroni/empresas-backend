module.exports = (sequelize, Sequelize, DataTypes) => {
    const Enterprise = sequelize.define(
      "enterprise", {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,      
          primaryKey: true
        },
        email_enterprise: {
          type: DataTypes.STRING
        },        
        facebook: {
          type: DataTypes.STRING
        },
        twitter: {
          type: DataTypes.STRING
        },
        linkedin: {
          type: DataTypes.STRING
        },
        phone: {
          type: DataTypes.STRING
        },
        own_enterprise: {
          type: DataTypes.BOOLEAN
        },
        enterprise_name: {
          type: DataTypes.STRING,
          unique: true
        },
        photo: {
          type: DataTypes.STRING
        },
        description: {
          type: DataTypes.STRING
        },
        city: {
          type: DataTypes.STRING
        },
        country: {
          type: DataTypes.STRING
        },
        value: {
          type: DataTypes.INTEGER
        },
        shares: {
          type: DataTypes.INTEGER
        },
        share_price: {
          type: DataTypes.INTEGER
        },
        own_shares: {
          type: DataTypes.INTEGER
        },
        
        enterprise_type_id: {
          type: DataTypes.INTEGER
          /**references: { model: 'enterprise_typeModel', key: 'id' },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          allowNull: true,**/
        }
      },
      {
        timestamps: true,
        underscrored: true,
        createdAt: "created_at",
        updatedAt: "updated_at"
      }
    );
  
    return Enterprise;
  };
  