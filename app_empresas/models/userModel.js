module.exports = (sequelize, Sequelize, DataTypes) => {
  const User = sequelize.define(
    "user", {
      id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      username: {
        type: DataTypes.STRING,
        unique: true
      },
      email: {
        type: DataTypes.STRING
      },
      password: {
        type: DataTypes.STRING
      },
      city: {
        type: DataTypes.STRING
      },
      country: {
        type: DataTypes.STRING
      },
      balance: {
        type: DataTypes.INTEGER
      },
      photo: {
        type: DataTypes.STRING
      },
      portfolio_value: {
        type: DataTypes.INTEGER
      },
      first_access: {
        type: DataTypes.BOOLEAN
      },
      super_angel: {
        type: DataTypes.BOOLEAN
      }
    },
    {
      timestamps: true,
      underscrored: true,
      createdAt: "created_at",
      updatedAt: "updated_at"
    }
  );

  return User;
};
