const config = require("../config/config.js");
const { Sequelize, DataTypes, Op } = require("sequelize");

const sequelize = new Sequelize(
  config.db.DB_NAME,
  config.db.DB_USER,
  config.db.DB_PASS,
  {
    host: config.db.DB_HOST,
    dialect: config.db.dialect,
    operatorsAliases: false,

    poll: {
      max: config.db.pool.max,
      min: config.db.pool.min,
      acquire: config.db.pool.acquire,
      idle: config.db.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.Op = Op;
db.sequelize = sequelize;


db.enterprise_type = require("./enterprise_typeModel.js")(sequelize, Sequelize, DataTypes);
db.enterprises = require("./enterpriseModel.js")(sequelize, Sequelize, DataTypes);
db.user = require("./userModel.js")(sequelize, Sequelize, DataTypes);
db.perfil = require("./perfilModel.js")(sequelize, Sequelize, DataTypes);

db.perfil.belongsToMany(db.user, {
  through: "user_perfis",
  foreignKey: "perfil_id",
  otherKey: "user_id"
});
db.user.belongsToMany(db.perfil, {
  through: "user_perfis",
  foreignKey: "user_id",
  otherKey: "perfil_id"
});

db.PERFIS = ["angel", "personal", "banks"];

module.exports = db;
