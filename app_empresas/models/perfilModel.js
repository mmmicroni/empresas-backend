module.exports = (sequelize, Sequelize, DataTypes) => {
  const Perfil = sequelize.define(
    "perfil", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING
      }
    },
    {
      timestamps: true,
      underscrored: true,
      createdAt: "created_at",
      updatedAt: "updated_at"
    }
  );

  return Perfil;
};
