module.exports = (sequelize, Sequelize, DataTypes) => {
    const Enterprise_type = sequelize.define(
      "enterprise_type", {
        id: {
          type: DataTypes.UUID,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        enterprise_type_name: {
          type: DataTypes.STRING,
          unique: true
        },
      },
      {
        timestamps: true,
        underscrored: true,
        createdAt: "created_at",
        updatedAt: "updated_at"
      }
    );
  
    return Enterprise_type;
  };
  