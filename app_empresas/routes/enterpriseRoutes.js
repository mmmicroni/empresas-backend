module.exports = app => {
    const enterpriseController = require("../controllers/enterpriseController.js");
  
    const router = require("express").Router();
  
    // Criar uma nova Enterprise
    router.post("/", enterpriseController.create);
  
    // Buscar todas as Enterprises
    router.get("/", enterpriseController.findAll);

    // Buscar uma Enterprise pelo seu id
    router.get("/:id", enterpriseController.findOne);
    
    //Busca Enterprise por Filtro
    router.get("/", enterpriseController.findAllFilter);
  
    app.use("/api/v1/enterprises", router);
  };
  