module.exports = {
    PORT: 8080,
  
    // CONEXAO COM BANCO
    db: {
      DB_HOST: "localhost",
      DB_USER: "microni",
      DB_PASS: "123456",
      DB_NAME: "empresa_db",
      dialect: "mysql",
  
      // PARA USO DO Sequelize
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    },
  
    // CHAVE
    auth: {
      secret: "desafio-ioasys-empresa-backend"
    }
  };
  