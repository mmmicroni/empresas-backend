const db = require("../models");
const PERFIS = db.PERFIS;
const User = db.user;

checkDuplicateUsernameOrEmail = (req, res, next) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  }).then(user => {
    if (user) {
      res.status(400).send({
        message: "ERRO! O NOME DO USUARIO JA EXISTE!"
      });
      return;
    }

    User.findOne({
      where: {
        email: req.body.email
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          message: "ERRO O EMAIL JA EXISTE!"
        });
        return;
      }

      next();
    });
  });
};

checkRolesExisted = (req, res, next) => {
  if (req.body.perfis) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!PERFIS.includes(req.body.perfis[i])) {
        res.status(400).send({
          message: "erro! O PERFIL NÃO EXISTE! = " + req.body.perfis[i]
        });
        return;
      }
    }
  }

  next();
};

const verifySignUp = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail,
  checkRolesExisted: checkRolesExisted
};

module.exports = verifySignUp;
