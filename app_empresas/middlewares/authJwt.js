const jwt = require("jsonwebtoken");
const config = require("../config/config.js");
const db = require("../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "TOKEN NÃO ENCONTRADO!"
    });
  }

  jwt.verify(token, config.auth.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "NÃO AUTORIZADO!"
      });
    }

    req.userId = decoded.id;

    next();
  });
};

isAngel = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getPerfis().then(perfis => {
      for (let i = 0; i < perfis.lenth; i++) {
        if (perfis[i].name === "angel") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "PRECISA SER INVESTIDOR ANJO!"
      });
      return;
    });
  });
};

isPersonal = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getPerfis().then(perfis => {
      for (let i = 0; i < roles.length; i++) {
        if (perfis[i].name === "personal") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "PRECISA SER INVESTIDOR PESSOAL!!"
      });
    });
  });
};

isPersonalOrAngel = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    user.getPerfis().then(perfis => {
      for (let i = 0; i < perfis.length; i++) {
        if (perfis[i].name === "personal") {
          next();
          return;
        }

        if (perfis[i].name === "angel") {
          next();
          return;
        }
      }

      res.status(403).send({
        message: "PRECISA SER INVESTIDOR PESSOAL OU ANJO!!"
      });
    });
  });
};

const authJwt = {
  verifyToken: verifyToken,
  isAngel: isAngel,
  isPersonal: isPersonal,
  isPersonalOrAngel: isPersonalOrAngel
};

module.exports = authJwt;
