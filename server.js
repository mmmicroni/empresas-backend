const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("./app_empresas/config/config.js");

const app = express();

const corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// application/json
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

// para o banco de dados
const db = require("./app_empresas/models");
const Perfil = db.perfil;
db.sequelize.sync().then(() => {
  initial(); // comentar este depois que rodar a primeira vez
});

// ioasys rota
app.get("/", (req, res) => {
  res.json({ message: "Olá, tudo muito bem? Este é o desafio da ioasys - empresa_backend!" });
});

// rotas da api
require("./app_empresas/routes/authRoutes")(app);
require("./app_empresas/routes/userRoutes")(app);
require("./app_empresas/routes/enterpriseRoutes")(app);

// setando a porta
const PORT = config.PORT;
app.listen(PORT, () => {
  console.log(" O Servidor está rodando na porta ${PORT}");
});

// comentar este depois que rodar a primeira vez
function initial() {
  Perfil.create({
    id: 1,
    name: "angel"
  });

  Perfil.create({
    id: 2,
    name: "personal"
  });

  Perfil.create({
    id: 3,
    name: "banks"
  });
}
